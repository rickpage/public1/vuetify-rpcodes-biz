import Vue from 'vue'
import Router from 'vue-router'
import Introduction from '../Introduction'
import Portfolio from '../Portfolio'
import Skills from '../Skills'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Introduction
    },
    {
      path: '/experience',
      name: 'experience',
      component: Skills
    },
    {
      path: '/portfolio',
      name: 'portfolio',
      component: Portfolio
    },
  ]
})
