import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import colors from 'vuetify/es5/util/colors'
// custom theme
let opts = {
  theme: {
    primary: colors.deepPurple.darken3,
    secondary: colors.indigo.base,
    accent: colors.indigo.lighten2,
    error: colors.blue.lighten1,
    info: colors.indigo.lighten4,

  }
}

// DEFAULT THEME:
opts = {}
Vue.use(Vuetify, opts)

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
